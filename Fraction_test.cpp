/*=============================================================================
Name: 		Eduardo Ortiz
			Brian X. Leon

e-mail:		eduardo3991@hotmail.com
			brian.leon6@gmail.com
=============================================================================*/

#include <iostream>
#include "Fraction.h"
#include <cstdlib>		//random numbers
#include <ctime>		//time function
using namespace std;

Fraction sumArray(Fraction[], int);	//sums an array of fractions
void sortFractionArray(Fraction[], int); //sorting for fractions array

int main()
{	
	Fraction a, b, result, sum;	
	int size = 10;

	srand(time(NULL));		//unique seed for random numbers
	
	//setting numerators and denominators for both fractions
	a.setNum((rand() % 9) + 1);
	a.setDenom((rand() % 9) + 1);
	b.setNum((rand() % 9) + 1);
	b.setDenom((rand() % 9) + 1);

	cout << "Let's work with fractions..." << endl << endl;

	cout << "The first fraction is:\t";

	a.print();

	cout << endl << "The second is:\t";

	b.print();

	result = a.add(b);

	cout << endl << endl << "The sum is:\t";

	result.print();

	result = a.sub(b);

	cout << endl << endl << "The difference is: \t";

	result.print();

	result = a.mul(b);

	cout << endl << endl << "The multiplication is: \t";

	result.print();

	result = a.div(b);

	cout << endl << endl << "The division is: \t";

	result.print();

	cout << endl << endl << "Now for a fraction array." << endl;

	Fraction array[size];

	//fills the array with random fractions
	for(int c = 0; c < size; c++)
	{
		array[c].setNum(((rand() % 9) + 1));

		array[c].setDenom(((rand() % 9) + 1));

		array[c].print();

		cout << endl;
	}

	cout << endl << "The array in ascending order is: " << endl;

	sortFractionArray(array, size);

	for(int c = 0; c < size; c++)
	{
		array[c].print();

		cout << endl;
	}

	cout << endl << "Its sum is: " << endl;

	sum = sumArray(array, size);

	sum.print();

	cout << endl;

	return 0;
}

//Function that sums the elements of a fraction array and returns the result
Fraction sumArray(Fraction array[], int size)
{
	Fraction accumulator;	//this will hold the summation

	//standard loop for adding stuff
	//it uses the .add function in the Fraction class to compute the sum
	for(int i = 0; i < size; i++)
		accumulator = accumulator.add(array[i]); 

	return accumulator;
}

//Function that sorts the elements of a fraction array in an ascending order
void sortFractionArray(Fraction array[], int size)
{
	if(size == 1) return;	//nothing to sort if it's just one element

	//classic variable for a selection sort
	Fraction swapper, minimum;
	int swap_pos = 0;			

	//follows the same loop save for the use of the .gt(greater than) function
	for(int a = 0; a < size - 1; a++)
	{
		minimum = array[a];

		for(int c = a + 1; c < size; c++)
			if(minimum.gt(array[c]))
			{
				minimum = array[c];
				swap_pos = c;
			}

		swapper = array[a];
		array[a] = minimum;
		array[swap_pos] = swapper;
	}
}